

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
        int counter = 0;

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
	    System.out.print("Can you guess it:");
        
        int guess = reader.nextInt(); //Read the user input
        while ((guess != number) && (guess != -1)){
            counter++;
            System.out.println("Sorry!");
            System.out.println("Type -1 to quit guess another");
            if (guess > number){
                System.out.println("Mine is less than your guess");
            }else 
                System.out.println("Mine is greater than your guess");
            guess = reader.nextInt();
        }         
            if (guess == number){
                System.out.println("Congratulations!");
                System.out.println("You won after "+ counter +" attemps");

            }else{
                System.out.println("Sorry");
                
	        }
		   
        
 		reader.close(); //Close the resource before exiting
    }

 
	
}

