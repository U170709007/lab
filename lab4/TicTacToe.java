import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);
        int move =0;
        while(move < 9){
            int row,col;
            do{		    
                System.out.print("Player 1 enter row number:");
		        row = reader.nextInt();
		        System.out.print("Player 1 enter column number:");
		        col = reader.nextInt();
            }while(!(row > 0 && row < 4 && col > 0 && col < 4 && board[row-1][col-1] == ' '));
            
		    board[row - 1][col - 1] = 'X';
            move++;	    
            printBoard(board);
            if (checkBoard(board, row -1 , col -1 )){
                System.out.print("PLayer 1 win");
                break;
            }
            

		    do{		    
                System.out.print("Player 2 enter row number:");
		        row = reader.nextInt();
		        System.out.print("Player 2 enter column number:");
		        col = reader.nextInt();
            }while(!(row > 0 && row < 4 && col > 0 && col < 4 && board[row-1][col-1] == ' '));
		    board[row - 1][col - 1] = 'O';
		    printBoard(board);
            if (checkBoard(board, row -1 , col -1 )){
               System.out.print("PLayer 2 win");
               break;
            }
        }
        reader.close();
    }    
    public static boolean checkBoard( char[][] board, int rowLast, int colLast){	
        char s = board[rowLast][colLast];
        boolean winFlag = true;
        for (int col = 0; col < 3; col++){
            if(board[rowLast][col] != s){
                winFlag = false;
                break;
            }
        }
        if(winFlag){
            return true;
        }
        winFlag = true;
        for (int row = 0; row < 3; row++){
            if(board[row][colLast] != s){
                winFlag = false;
                break;
            }
        }
        if(winFlag){
            return true;
        }
        if(rowLast == colLast) {
            winFlag = true;
            for (int a = 0; a < 3; a++){
                if(board[a][a] != s){
                    winFlag = false;
                    break;
                }
            }
        
            if(winFlag){
                return true;
            }
        }
        if (rowLast + colLast == 2){
            winFlag = true;
            for(int b = 0; b < 3;b++){
                if(board[b][2-b] != s){
                    winFlag = false;
                    break;
                }
            }
        }        
        
        if(winFlag){
            return true;
        }
        return false;
    }

    
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
