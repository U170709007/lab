public class GCDLoop{
    
    public static void main(String[] args){
        int a= Integer.parseInt(args[0]);
        int b= Integer.parseInt(args[1]);
        int c;
        while(a>b && a>0 && b>0){
            c = a % b;
            a = b;
            b = c;
        }
        System.out.println(a);
    }
}            
